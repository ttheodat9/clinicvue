var express = require('express');
var mongodb = require('mongodb');
var mongoclient = mongodb.MongoClient;
var router = express.Router();
require('dotenv').config();
var url = 'mongodb+srv://clinicdb:clinic21@cluster0.w0s1g.mongodb.net/tericlinic?retryWrites=true&w=majority';

var options = {
   replSet: {
     sslValidate: false
   }
  }

/* GET home page. */
router.get('/', function (req, res, next) {
  mongoclient.connect(url + '/mymongodb', options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
      let dbObject = db.db('clinicdb');

      dbObject.collection('staff').find().toArray((error, result) => {
        if(error) throw error;
        db.close();
        res.send(JSON.stringify(result));

      })
  });
});

module.exports = router;